import pandas as pd

def flatten(entry):
    for k in filter(lambda k: type(entry[k])==dict, entry):
        entry[k]=entry[k].get('code', entry[k].get('id'))
    for k in list(filter(lambda k: type(entry[k])==list, entry)):
        del entry[k]

def explode(entry, key):
    entry.update({p['code']:p['value'] for p in entry[key]})
    del entry[key]

def listBatches(client, filterMap):
    batches=c.get('listBatches', json={'filterMap':filterMap})
    return pd.json_normalize(batches)

def listComponents(client, filterMap):
    """
    Get components using the `listComponents` API.

    The contents of all components, properties and children are expanded
    individually using the `pd.json_normalize` function. For properties,
    any array values are exploded. This assumes that they contain the same
    length. The properties are returned separately in case the array lenghts
    do not match up.

    Object results are handled by pivoting their DataFrame and dropping any
    NaN columns. The columns are then renamed to `code.objectCode` and merged
    to the single type DataFrame manipulated as above.

    The component id column is set as the index for all returned DataFrames. This
    makes it possible to merge the DataFrames together if needed.

    Parameters
    ----------
    client : itkdb.Client
        The client object.
    filterMap : dict
        Filter for returned components. See API documentation.

    Returns
    -------
    components : pd.DataFrame
        DataFrame of components.
    properties : pd.DataFrame
        DataFrame of properties.
    children : pd.DataFrame
        DataFrame of children.
    """
    res=client.get('listComponents',json={'filterMap':filterMap})
    res=list(res)

    # Components
    components = pd.json_normalize(res)
    components.drop(columns=['properties'], inplace=True)

    properties=pd.json_normalize(res, record_path='properties', meta='id')
    properties_flat=properties.pivot(index='id', columns='code', values='value')
    components=components.merge(properties_flat, on='id', suffixes=('', '_property'))

    # Children
    children=pd.json_normalize(res, record_path='children', meta='id', meta_prefix='parent.')

    return components, properties, children

def listComponentsByProperty(client, project, propertyFilter, componentType=None):
    filterMap={'project':'S', 'propertyFilter':propertyFilter}
    if componentType!=None:
        filterMap['componentType']=componentType
    r=client.get('listComponentsByProperty',json=filterMap)
    r=list(r)
    for component in r:
        explode(component, 'properties')
        flatten(component)

    return pd.DataFrame.from_records(r)

def getComponents(client, components):
    res=client.get('getComponentBulk',json={'component':components})

    #
    # Components
    components = pd.json_normalize(res)
    components.drop(columns=['properties'], inplace=True)

    properties=pd.json_normalize(res, record_path='properties', meta='id')
    properties_flat=properties.pivot(index='id', columns='code', values='value')
    components=components.merge(properties_flat, on='id', suffixes=('', '_property'))

    # Children
    children=pd.json_normalize(res, record_path='children', meta='id', meta_prefix='parent.')

    # Tests
    tests=pd.json_normalize(res, record_path='tests', meta='id', meta_prefix='component.')
    tests.drop(columns=['testRuns'], inplace=True)

    testRuns=pd.json_normalize(res, record_path=['tests','testRuns'],meta=['id',['tests','id']], meta_prefix='component.')
    testRuns=testRuns.merge(tests, left_on=['component.id','component.tests.id'], right_on=['component.id','id'], suffixes=('', '_test'))

    return components,children,testRuns

def getComponentTypeStages(client, componentType):
    r=client.get('getComponentTypeByCode',json={'project':'S', 'code':componentType})
    for stage in r['stages']:
        flatten(stage)
    return pd.DataFrame.from_records(r['stages'])

def listTestRuns(client, componentType=None, testType=None, component=None, stage=None):
    #
    # Look up
    if component is None:
        # Look up by test type
        if testType is None and componentType is None:
            raise ValueError("testType and componentType cannot be None when looking up by testType")
        args={
            'project':'S',
            'testType':testType,
            'componentType':componentType,
            }
        if stage is not None:
            args['stage']=stage if type(stage)==list else [stage]
        r=client.get('listTestRunsByTestType',json=args)
    else:
        # Look up by component
        args={'filterMap':{'serialNumber': component,'state': 'ready'}}
        if testType is not None:
            args['filterMap']['testType']=testType if type(testType)==list else [testType]
        if stage    is not None:
            args['filterMap']['stage'   ]=stage    if type(stage   )==list else [stage   ]

        r=client.get('listTestRunsByComponent',json=args)

    testRuns=pd.json_normalize(r)

    # Apply extra filters if necessary. Need a non-zero data frame to ensure
    # that the necessary columns are there.
    if len(testRuns.index)>0:
        # Return non-deleted test runs. This is redundant if listing by
        # component, but necessary if listing by test type. Keep here
        # for some symmetry between the two modes.
        testRuns=testRuns[testRuns['state']!='deleted']

    return testRuns

def getTestRuns(client, testRuns, explode_results=True):
    """
    Get the test runs using the `getTestRunBulk` API.

    The contents of all tests, results and properties are expanded
    individually using the `pd.json_normalize` function. For results
    and properties, any array values are exploded. This assumes that
    they contain the same length. The results and properties are returned
    separately in case the array lenghts do not match up.

    Object results are handled by pivoting their DataFrame and dropping any
    NaN columns. The columns are then renamed to `code.objectCode` and merged
    to the single type DataFrame manipulated as above.

    The test id column is set as the index for all returned DataFrames. This
    makes it possible to merge the DataFrames together if needed.

    Parameters
    ----------
    client : itkdb.Client
        The client object.
    testRuns : list
        List of test run IDs.
    explode_results : bool
        Explore the array results into individual rows.

    Returns
    -------
    testRuns : pd.DataFrame
        DataFrame of test runs.
    results : pd.DataFrame
        DataFrame of results.
    properties : pd.DataFrame
        DataFrame of properties.
    """
    res=client.get('getTestRunBulk', json={'testRun':testRuns})

    if len(res)==0:
        return pd.DataFrame(),pd.DataFrame(),pd.DataFrame()

    testRuns=pd.json_normalize(res).set_index('id')
    testRuns['date']=pd.to_datetime(testRuns['date'])
    testRuns.drop(columns=['results','properties'], inplace=True)

    results=pd.json_normalize(res, record_path='results', meta='id')

    # Handle single valued results
    results_single=results[results['dataType']!='object']
    if len(results_single.index)==0: # No single valued results, skip
        # The "empty" DataFrame of single-valued columns should not actually
        # be empty as we still want a row per test run. This creates a valid
        # result, where the "value" columns are empty (non-existent).
        #
        # The results_flat will also be merged (left) with the object results,
        # so if this is empty then also the final result will be empty too.
        results_flat=pd.DataFrame(columns=['id'], data=results['id'].unique())
    else:
        arrays=results_single[results_single['valueType']=='array']['code'].unique().tolist()
        results_flat=results_single.pivot(index='id', columns='code', values='value')
        if explode_results and len(arrays)>0:
            results_flat=results_flat.explode(arrays)

    # Handle object results
    results_object=results[results['dataType']=='object']
    valuecol=[c for c in results_object.columns if c.startswith('value.')]
    results_object_flat=results_object.pivot(index='id', columns='code', values=valuecol)
    results_object_flat=results_object_flat.dropna(axis=1, how='all')
    results_object_flat.columns=results_object_flat.columns.reorder_levels(order=[1,0]).map('.'.join).str.replace('.value.','.')
    results_flat=results_flat.merge(results_object_flat, on='id', how='left')

    properties=pd.json_normalize(res, record_path='properties', meta='id')
    arrays=properties[properties['valueType']=='array']['code'].unique().tolist()
    properties_flat=properties.pivot(index='id', columns='code', values='value')
    if len(arrays)>0:
        properties_flat=properties_flat.explode(arrays)

    return testRuns,results_flat,properties_flat
