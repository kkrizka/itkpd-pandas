v0.1.0 (upcoming):
    - Rework `listComponents` to leverage `pd.json_normalize` more directly. Not backwards compatible!
    - Rework `getComponents` to leverage `pd.json_normalize` more directly. Not backwards compatible!
    - Rework `getTestRuns` to leverage `pd.json_normalize` more directly. Not backwards compatible!
    - Rework `listTestRuns` to leverage `pd.json_normalize` directly. Not backwards compatible!
    - Add `batch` module with batch functions.
